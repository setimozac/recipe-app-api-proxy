# recipe-app-api-proxy

NGINX proxy server for recipe app API

## Usage

### Environment variables
* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - Host name of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

